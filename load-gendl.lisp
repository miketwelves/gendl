(in-package :common-lisp-user)

(load "/usr/src/app/quicklisp/setup")

(push "/usr/src/app/gendl/" ql:*local-project-directories*)

(ql:register-local-projects)

(ql:quickload :gendl)

;;(gendl:start-gendl!)
