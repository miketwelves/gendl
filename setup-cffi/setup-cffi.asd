;;;; -*- coding: utf-8 -*-

(asdf:defsystem #:setup-cffi :description
 "The Gendl® setup-cffi Subsystem" :author "Genworks International"
 :license "Affero Gnu Public License (http://www.gnu.org/licenses/)"
 :serial t :version "20210427" :depends-on ("cffi")
 :defsystem-depends-on nil :components ((:file "source/setup")))
