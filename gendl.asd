;;;; -*- coding: utf-8 -*-

(asdf:defsystem #:gendl :description
 "The Gendl® gendl Subsystem" :author "Genworks International"
 :license "Affero Gnu Public License (http://www.gnu.org/licenses/)"
 :serial t :version "20210427" :depends-on
 (:gwl-graphics :geysr :robot :yadd :cl-lite) :defsystem-depends-on
 nil :components nil)
